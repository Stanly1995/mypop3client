﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyMailLibrary;

namespace MyPOP3Client
{
    public partial class MainForm : Form
    {
        private POP3 pop;
        private TcpClient server;
        private List<string> statusItems;
        private Cursor cr = Cursor.Current;
        private bool connect;
        public MainForm()
        {
            InitializeComponent();
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            cr = Cursors.WaitCursor;
            Status.Clear();
            pop = new POP3(ServerName.Text, Login.Text, Password.Text, int.Parse(Port.Text));
            pop.Connect();
            connect = pop.connect;
            server = pop.server;
            statusItems = pop.statusItems;
            foreach (string items in statusItems)
            {
                Status.Text += items+"\r\n";
            }
            if (connect)
            {
                ChangeEnabled();
            }
            cr = Cursors.Arrow;
        }

        private void DisconnectButton_Click(object sender, EventArgs e)
        {
            cr = Cursors.WaitCursor;
            pop.Disconnect();
            connect = false;
            ChangeEnabled();
            statusItems = pop.statusItems;
            Status.Text += statusItems[statusItems.Count - 1];
            Message.Clear();
            MailNum.Clear();
            Password.Clear();
            cr = Cursors.Arrow;
        }

        private void ChangeEnabled()
        {
            ConnectButton.Enabled = !ConnectButton.Enabled;
            DisconnectButton.Enabled = !DisconnectButton.Enabled;
            RetrieveButton.Enabled = !RetrieveButton.Enabled;
        }

        private void RetrieveButton_Click(object sender, EventArgs e)
        {
            cr = Cursors.WaitCursor;
            pop.Retrieve(MailNum.Text);
            Message.Text = pop.message;
            cr = Cursors.Arrow;
        }
    }
}
