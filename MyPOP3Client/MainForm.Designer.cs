﻿namespace MyPOP3Client
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServerName = new System.Windows.Forms.TextBox();
            this.Login = new System.Windows.Forms.TextBox();
            this.Password = new System.Windows.Forms.TextBox();
            this.Port = new System.Windows.Forms.TextBox();
            this.Message = new System.Windows.Forms.TextBox();
            this.MailNum = new System.Windows.Forms.TextBox();
            this.Status = new System.Windows.Forms.TextBox();
            this.ServerNameLabel = new System.Windows.Forms.Label();
            this.PortLabel = new System.Windows.Forms.Label();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.LoginLabel = new System.Windows.Forms.Label();
            this.MessageLabel = new System.Windows.Forms.Label();
            this.MailNumLabel = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.DisconnectButton = new System.Windows.Forms.Button();
            this.RetrieveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ServerName
            // 
            this.ServerName.Location = new System.Drawing.Point(102, 17);
            this.ServerName.Name = "ServerName";
            this.ServerName.Size = new System.Drawing.Size(244, 22);
            this.ServerName.TabIndex = 0;
            this.ServerName.Text = "pop.meta.ua";
            // 
            // Login
            // 
            this.Login.Location = new System.Drawing.Point(102, 58);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(244, 22);
            this.Login.TabIndex = 1;
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(102, 102);
            this.Password.Name = "Password";
            this.Password.PasswordChar = '*';
            this.Password.Size = new System.Drawing.Size(244, 22);
            this.Password.TabIndex = 2;
            // 
            // Port
            // 
            this.Port.Location = new System.Drawing.Point(102, 145);
            this.Port.Name = "Port";
            this.Port.Size = new System.Drawing.Size(244, 22);
            this.Port.TabIndex = 3;
            this.Port.Text = "995";
            // 
            // Message
            // 
            this.Message.Location = new System.Drawing.Point(15, 212);
            this.Message.Multiline = true;
            this.Message.Name = "Message";
            this.Message.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Message.Size = new System.Drawing.Size(552, 169);
            this.Message.TabIndex = 4;
            // 
            // MailNum
            // 
            this.MailNum.Location = new System.Drawing.Point(102, 407);
            this.MailNum.Name = "MailNum";
            this.MailNum.Size = new System.Drawing.Size(244, 22);
            this.MailNum.TabIndex = 5;
            // 
            // Status
            // 
            this.Status.Location = new System.Drawing.Point(15, 483);
            this.Status.Multiline = true;
            this.Status.Name = "Status";
            this.Status.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Status.Size = new System.Drawing.Size(552, 106);
            this.Status.TabIndex = 6;
            // 
            // ServerNameLabel
            // 
            this.ServerNameLabel.AutoSize = true;
            this.ServerNameLabel.Location = new System.Drawing.Point(12, 20);
            this.ServerNameLabel.Name = "ServerNameLabel";
            this.ServerNameLabel.Size = new System.Drawing.Size(50, 17);
            this.ServerNameLabel.TabIndex = 7;
            this.ServerNameLabel.Text = "Server";
            // 
            // PortLabel
            // 
            this.PortLabel.AutoSize = true;
            this.PortLabel.Location = new System.Drawing.Point(12, 148);
            this.PortLabel.Name = "PortLabel";
            this.PortLabel.Size = new System.Drawing.Size(34, 17);
            this.PortLabel.TabIndex = 8;
            this.PortLabel.Text = "Port";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Location = new System.Drawing.Point(12, 105);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(69, 17);
            this.PasswordLabel.TabIndex = 9;
            this.PasswordLabel.Text = "Password";
            // 
            // LoginLabel
            // 
            this.LoginLabel.AutoSize = true;
            this.LoginLabel.Location = new System.Drawing.Point(12, 61);
            this.LoginLabel.Name = "LoginLabel";
            this.LoginLabel.Size = new System.Drawing.Size(43, 17);
            this.LoginLabel.TabIndex = 10;
            this.LoginLabel.Text = "Login";
            // 
            // MessageLabel
            // 
            this.MessageLabel.AutoSize = true;
            this.MessageLabel.Location = new System.Drawing.Point(12, 183);
            this.MessageLabel.Name = "MessageLabel";
            this.MessageLabel.Size = new System.Drawing.Size(65, 17);
            this.MessageLabel.TabIndex = 11;
            this.MessageLabel.Text = "Message";
            // 
            // MailNumLabel
            // 
            this.MailNumLabel.AutoSize = true;
            this.MailNumLabel.Location = new System.Drawing.Point(12, 410);
            this.MailNumLabel.Name = "MailNumLabel";
            this.MailNumLabel.Size = new System.Drawing.Size(85, 17);
            this.MailNumLabel.TabIndex = 12;
            this.MailNumLabel.Text = "Mail number";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(12, 453);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(48, 17);
            this.StatusLabel.TabIndex = 13;
            this.StatusLabel.Text = "Status";
            // 
            // ConnectButton
            // 
            this.ConnectButton.Location = new System.Drawing.Point(374, 16);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(193, 23);
            this.ConnectButton.TabIndex = 14;
            this.ConnectButton.Text = "Connect";
            this.ConnectButton.UseVisualStyleBackColor = true;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // DisconnectButton
            // 
            this.DisconnectButton.Enabled = false;
            this.DisconnectButton.Location = new System.Drawing.Point(374, 57);
            this.DisconnectButton.Name = "DisconnectButton";
            this.DisconnectButton.Size = new System.Drawing.Size(193, 23);
            this.DisconnectButton.TabIndex = 15;
            this.DisconnectButton.Text = "Disconnect";
            this.DisconnectButton.UseVisualStyleBackColor = true;
            this.DisconnectButton.Click += new System.EventHandler(this.DisconnectButton_Click);
            // 
            // RetrieveButton
            // 
            this.RetrieveButton.Enabled = false;
            this.RetrieveButton.Location = new System.Drawing.Point(374, 405);
            this.RetrieveButton.Name = "RetrieveButton";
            this.RetrieveButton.Size = new System.Drawing.Size(193, 23);
            this.RetrieveButton.TabIndex = 16;
            this.RetrieveButton.Text = "Retrieve";
            this.RetrieveButton.UseVisualStyleBackColor = true;
            this.RetrieveButton.Click += new System.EventHandler(this.RetrieveButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 601);
            this.Controls.Add(this.RetrieveButton);
            this.Controls.Add(this.DisconnectButton);
            this.Controls.Add(this.ConnectButton);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.MailNumLabel);
            this.Controls.Add(this.MessageLabel);
            this.Controls.Add(this.LoginLabel);
            this.Controls.Add(this.PasswordLabel);
            this.Controls.Add(this.PortLabel);
            this.Controls.Add(this.ServerNameLabel);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.MailNum);
            this.Controls.Add(this.Message);
            this.Controls.Add(this.Port);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.Login);
            this.Controls.Add(this.ServerName);
            this.Name = "MainForm";
            this.Text = "POP3 Client";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ServerName;
        private System.Windows.Forms.TextBox Login;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.TextBox Port;
        private System.Windows.Forms.TextBox Message;
        private System.Windows.Forms.TextBox MailNum;
        private System.Windows.Forms.TextBox Status;
        private System.Windows.Forms.Label ServerNameLabel;
        private System.Windows.Forms.Label PortLabel;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Label LoginLabel;
        private System.Windows.Forms.Label MessageLabel;
        private System.Windows.Forms.Label MailNumLabel;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.Button DisconnectButton;
        private System.Windows.Forms.Button RetrieveButton;
    }
}

