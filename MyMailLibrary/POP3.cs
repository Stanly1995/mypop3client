﻿using System;
using System.Net.Sockets;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;
using System.Text;
using System.Collections.Generic;

namespace MyMailLibrary
{
    public class POP3
    {
        #region fields
        private TcpClient _server;
        private string _data;
        private string _crlf = "\r\n";
        private SslStream _sslStream;
        private StreamReader _sslReader;
        private StreamWriter _sslWriter;
        private List<string> _statusItems = new List<string>();
        private string _serv;
        private string _client;
        private string _pass;
        private int _port;
        private StringBuilder _message = new StringBuilder();
        private bool _connect;

        public bool connect
        {
            get { return _connect; }
        }
        public List<string> statusItems
        {
            get { return _statusItems; }
        }
        public TcpClient server
        {
            get { return _server; }
        }

        public string message
        {
            get { return _message.ToString(); }
        }
        #endregion

        #region ctor
        public POP3(string serv, string client, string pass, int port)
        {
            _serv = serv;
            _client = client;
            _pass = pass;
            _port = port;
        }
        #endregion

        #region Methods
        public void Connect()
        {
            ConnectPrivate();
        }

        private void ConnectPrivate()
        {
            statusItems.Clear();
            // Create connection
            _server = new TcpClient();
            _server.Connect(_serv, _port);
            // SSL
            _sslStream = new SslStream(_server.GetStream());
            {
                _sslStream.AuthenticateAsClient(_serv, new X509CertificateCollection(), SslProtocols.Tls, true);
                _sslReader = new StreamReader(_sslStream, Encoding.GetEncoding(1251));
                _sslWriter = new StreamWriter(_sslStream, Encoding.GetEncoding(1251)) { AutoFlush = true };
                {
                    try
                    {
                        _statusItems.Add(_sslReader.ReadLine());
                        _data = "USER " + _client + _crlf;
                        _sslWriter.Write(_data);
                        _statusItems.Add(_sslReader.ReadLine());
                        _data = "PASS " + _pass + _crlf;
                        _sslWriter.Write(_data);
                        _statusItems.Add(_sslReader.ReadLine());
                        _data = "STAT" + _crlf;
                        _sslWriter.Write(_data);
                        _statusItems.Add(_sslReader.ReadLine());
                        _connect = true;
                    }
                    catch (InvalidOperationException err)
                    {
                        _statusItems.Add("Error: " + err.ToString());
                        _connect = false;
                    }
                }
            }
        }

        public void Disconnect()
        {
            DisconnectPrivate();
        }

        private void DisconnectPrivate()
        {
            _data = "QUIT" + _crlf;
            _sslWriter.Write(_data);
            _statusItems.Add(_sslReader.ReadLine());
            _connect = false;
            _sslWriter.Close();
            _sslReader.Close();
        }

        public void Retrieve(string num)
        {
            RetrievePrivate(num);
        }

        private void RetrievePrivate(string num)
        {
            string szTemp;
            _message.Clear();
            try
            {
                _data = "RETR " + num + _crlf;

                _sslWriter.Write(_data);

                szTemp = _sslReader.ReadLine();
                if (szTemp == null)
                    throw new InvalidOperationException();

                if (szTemp[0] != '-')
                {
                    while (szTemp != ".")
                    {
                        _message.Append(szTemp);
                        _message.Append(_crlf);
                        szTemp = _sslReader.ReadLine();
                    }
                }
                else
                {
                    _statusItems.Add(szTemp);
                }
            }
            catch (InvalidOperationException err)
            {
                _statusItems.Add("Error: " + err.ToString());
            }
        }
        #endregion
    }
}